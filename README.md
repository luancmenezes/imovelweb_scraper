# Zap Imóveis - Scraper
A scraper that gathers data from [Imovel Web](https://www.imovelweb.com.br/i) website.

## Installation

|Major requirements|
|-|
|[Python 3.6](https://www.python.org/)|
|[Scrapy](https://scrapy.org/) |

