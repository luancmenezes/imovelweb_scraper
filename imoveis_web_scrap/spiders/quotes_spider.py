import scrapy
from scrapy.http.request import Request
from datetime import *

def prepare_struct(elements):
    array = []
    for pos, elem in enumerate(elements):
        dic = {}
        try:
            if 'm' in elem:
                dic['m_square'] = elem.replace('m','').replace('²','')
            elif 'm' in elements[pos+1]:
                dic['bath'] = elem
            else:
                dic['room'] = elem
        except:
            dic['bath'] = elem
        array.append(dic)
    return array

class ImoveisSpider(scrapy.Spider):
    name = "imoveis"
    start_urls = [
        'https://www.imovelweb.com.br/apartamentos-aluguel-salvador-ba.html'
    ]

    def parse_data(self,response):
        item = response.meta["item"]
        datas = response.xpath('//h5[contains(@class,section-date) and contains(@class,css-float-r)]/text()').extract()
        data = datas.pop()
        try:
            dia = [int(s) for s in data.split() if s.isdigit()][0] 
        except:
            dia = None
        if dia:
            date = datetime.today() - timedelta(days=dia)
            item['posted'] = "{}/{}/{}".format(date.day,date.month,date.year)
        else:
            item['posted'] = None
        yield item


    def parse(self, response):
        datas_ = []
        count = -1
        
        for block in response.xpath('//li[contains(@class,aviso-desktop) and contains(@class,aviso) and contains(@class,aviso--superdestacado) ]'):
            neighborhoods = block.xpath('//div[@class="aviso-data-content"]//span[contains(@class, dl-aviso-link) and contains(@class, aviso-data-location)]//span/text()').extract()
            datas = block.xpath('//ul[contains(@class,aviso-data-features) and contains(@class,dl-aviso-link)]//li[@class="aviso-data-features-value"]/text()').extract()
            salarios = block.xpath('//span[@class="aviso-data-price-value"]/text()').extract()
            pages = response.xpath('//h4[@class="aviso-data-title"]//a/@href').extract()
            break
            
        for data in datas:
            x = data.replace('\t','').replace('\n','')
            if x != '':
                datas_.append(x)
        datas_ = prepare_struct(datas_)

        for x in range(len(neighborhoods)):
            item = {}
            ngh = neighborhoods.pop()
            ngh = ngh.split(',')
            item['city']  = ngh.pop()
            item['neighborhood'] =  ngh.pop()
            item['salarios'] = salarios.pop().replace('R$','')
            for opt in ['bath','room','m_square']:
                try:
                    item_ = datas_.pop()
                    item[opt] = item_[opt]
                except:
                    item[opt] = None
            
            page = response.urljoin(pages.pop())
            yield Request(page, callback=self.parse_data, meta={"item": item})
           


        link = response.xpath('//div[contains(@class,pagination) and contains(@class,pagination-centered)]//ul//li[@class="pagination-action-next "]//a/@href')
        url = response.urljoin(link.extract()[0])
        print ("AQUUUIIIII")
        print (url)
        yield Request(url, callback=self.parse)
